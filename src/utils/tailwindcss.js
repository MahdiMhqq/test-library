const defaultTheme = {
  varPrefix: "--w",
  colors: {
    varPrefix: "c",
    // TailwindCSS default Theme colors
    gray: {
      active: "#4B5563",
      default: "#6B7280",
      hover: "#9CA3AF",
    },
    red: {
      active: "#DC2626",
      default: "#EF4444",
      hover: "#F87171",
    },
    orange: {
      active: "#EA580C",
      default: "#F97316",
      hover: "#FB923C",
    },
    yellow: {
      active: "#D97706",
      default: "#F59E0B",
      hover: "#FBBF24",
    },
    green: {
      active: "#059669",
      default: "#10B981",
      hover: "#34D399",
    },
    teal: {
      active: "#0D9488",
      default: "#14B8A6",
      hover: "#2DD4BF",
    },
    cyan: {
      active: "#0891B2",
      default: "#06B6D4",
      hover: "#22D3EE",
    },
    lightBlue: {
      active: "#0284C7",
      default: "#0EA5E9",
      hover: "#38BDF8",
    },
    blue: {
      active: "#2563EB",
      default: "#3B82F6",
      hover: "#60A5FA",
    },
    indigo: {
      active: "#4F46E5",
      default: "#6366F1",
      hover: "#818CF8",
    },
    purple: {
      active: "#7C3AED",
      default: "#8B5CF6",
      hover: "#A78BFA",
    },
    pink: {
      active: "#DB2777",
      default: "#EC4899",
      hover: "#F472B6",
    },
    rose: {
      active: "#E11D48",
      default: "#F43F5E",
      hover: "#FB7185",
    },

    // Mandatory theme colors
    primary: {
      active: "#a6e3e3",
      default: "#b8fcfc",
      hover: "#cdfdfd",
    },
    secondary: {
      active: "#d7357b",
      default: "#ef3b89",
      hover: "#f476ac",
    },
    accent: {
      active: "#8cda97",
      default: "#9bf2a8",
      hover: "#b9f6c2",
    },
    surface: {
      active: "#404345",
      default: "#474A4D",
      hover: "#7e8082",
    },
    info: {
      active: "#8fb3d3",
      default: "#9FC7EA",
      hover: "#bcd8f0",
    },
    success: {
      active: "#30d2bf",
      default: "#35E9D4",
      hover: "#72f0e1",
    },
    warning: {
      active: "#b0870d",
      default: "#C4960E",
      hover: "#d6b656",
    },
    error: {
      active: "#cf5265",
      default: "#E65B70",
      hover: "#ee8c9b",
    },
    base: {
      active: "#241a27",
      default: "#281D2B",
      hover: "#69616b",
    },
    white: {
      active: "#FFF",
      default: "#FFF",
      hover: "#FFF",
    },
    black: {
      active: "#2b2b2b",
      default: "#2b2b2b",
      hover: "#2b2b2b",
    },
  },
  configs: {
    varPrefix: "con",
    defaultRadius: 0.5,
  },
};

const generateColors = () => {
  const generatedColorsObj = {};
  const colors = defaultTheme.colors;
  colors.varPrefix = "--w-c";

  Object.entries(colors).map(([key, _]) => {
    if (key !== "varPrefix") {
      generatedColorsObj[key] = `var(${colors.varPrefix}-${key}-default)`;
      generatedColorsObj[
        `${key}-hover`
      ] = `var(${colors.varPrefix}-${key}-hover)`;
      generatedColorsObj[
        `${key}-active`
      ] = `var(${colors.varPrefix}-${key}-active)`;
    }
  });

  return generatedColorsObj;
};

module.exports = {
  generateColors
}
