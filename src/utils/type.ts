export type DeepP<T, P> = {
  [K in keyof T]: DeepP<T[K], P> | P;
};

export type OneLevelP<T, P> = {
  [K in keyof T]: T[K] | P;
};
