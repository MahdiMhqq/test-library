export function convertRemToPx(rem: string | number) {
  const numberRem =
    typeof rem === "string"
      ? Number(rem.trim().toLowerCase().replaceAll("rem", ""))
      : rem;
  if (!isNaN(numberRem) && document && document?.documentElement) {
    return (
      numberRem *
      parseFloat(getComputedStyle(document.documentElement).fontSize)
    );
  } else return null;
}

export function convertPxToRem(px: string | number) {
  const numberPX =
    typeof px === "string"
      ? Number(px.trim().toLowerCase().replaceAll("px", ""))
      : px;
  if (!isNaN(numberPX) && document && document?.documentElement) {
    return (
      parseFloat(getComputedStyle(document.documentElement).fontSize) / numberPX
    );
  } else return null;
}

export function convertToRem(value: string | number) {
  if (typeof value === "string") {
    if (value.toLowerCase().includes("px")) return convertPxToRem(value);
    else if (value.toLowerCase().includes("rem")) {
      const cleanValue = value.trim().toLowerCase().replaceAll("rem", "");
      const numberValue = Number(cleanValue);
      if (!isNaN(numberValue)) return numberValue;
      else return null;
    } else return null;
  } else return value;
}
