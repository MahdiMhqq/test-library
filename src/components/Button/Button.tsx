import React, { HTMLAttributes, useContext } from "react";
import cslx from "clsx";

import { IColorPalette } from "../ThemeProvider";
import { ThemeContext } from "../ThemeProvider/ThemeProvider";

export interface IButtonProps
  extends Omit<HTMLAttributes<HTMLButtonElement>, "className" | "style"> {
  className?: string;
  children?: React.ReactNode;

  // color schema
  variant?: Exclude<keyof IColorPalette, "varPrefix">;

  // kind
  solid?: boolean;
  ghost?: boolean;
  border?: boolean;

  // for Icon Buttons
  icon?: boolean;

  // rounded
  rounded?: boolean;

  // disabled button
  disable?: boolean;

  // style attribute
  style?: React.CSSProperties;

  // completely override styles
  override?: string;
}

function Button({
  className = "",
  children,
  variant = "primary",
  solid,
  ghost,
  border,
  rounded,
  disable,
  style,
  override,
  icon,
  ...rest
}: IButtonProps) {
  //THEME CONTEXT
  const theme = useContext(ThemeContext);
  const defaultRadius = theme.configs?.defaultRadius;

  return (
    <button
      role="button"
      className={cslx([
        typeof override !== "string" && [
          icon ? "p-3" : "px-4 py-3",
          solid && [
            disable && `bg-gray hover:bg-gray-hover cursor-not-allowed`,
            !disable &&
              `bg-${variant} hover:bg-${variant}-hover active:bg-${variant}-active`,
          ],
          ghost && [
            disable && "bg-transparent hover:bg-gray cursor-not-allowed",
            !disable &&
              `bg-transparent hover:bg-${variant} active:bg-${variant}-active`,
          ],
          border && [
            disable &&
              "border-2 border-gray bg-transparent text-gray hover:bg-gray hover:text-white cursor-not-allowed",
            !disable &&
              `border-2 border-${variant} bg-transparent hover:bg-${variant} active:bg-${variant}-active text-${variant} hover:text-white`,
          ],
          rounded && "rounded-full",
        ],
        typeof override === "string" && override,
        className,
      ])}
      style={{
        ...style,
        borderRadius:
          typeof override !== "string" && !rounded
            ? defaultRadius + "rem"
            : undefined,
      }}
      {...rest}
    >
      {children}
    </button>
  );
}

export default Button;
