import React from "react";
import { Meta, StoryFn } from "@storybook/react";
import { within, userEvent } from "@storybook/testing-library";
import { expect } from "@storybook/jest";
import Button from "../Button";

// More on default export: https://storybook.js.org/docs/react/writing-stories/introduction#default-export
const ButtonMeta = {
  title: "Basics/Button",
  component: Button,
} as Meta<typeof Button>;

export default ButtonMeta;

// More on component templates: https://storybook.js.org/docs/react/writing-stories/introduction#using-args
const Template: StoryFn<typeof Button> = (args) => <Button {...args} />;

export const Primary = Template.bind({});
Primary.args = {
  children: "Button",
  variant: "primary",
};
Primary.play = async ({ args, canvasElement }) => {
  //Get Elements
  const canvas = within(canvasElement);
  const button = await canvas.findByRole("button");

  //Action
  userEvent.click(button);

  //Assertion
  expect(args.onClick).toHaveBeenCalled();
};

export const SolidDisable = Template.bind({});
SolidDisable.args = {
  children: "Button",
  variant: "primary",
  disable: true,
};

export const Secondary = Template.bind({});
Secondary.args = {
  children: "Button",
  variant: "secondary",
};

export const Info = Template.bind({});
Info.args = {
  children: "Button",
  variant: "info",
};

export const Border = Template.bind({});
Border.args = {
  children: "Button",
  border: true,
  variant: "secondary",
};

export const BorderDisabled = Template.bind({});
BorderDisabled.args = {
  children: "Button",
  border: true,
  variant: "secondary",
  disable: true,
};

export const Ghost = Template.bind({});
Ghost.args = {
  children: "Button",
  ghost: true,
  variant: "secondary",
};

export const GhostDisabled = Template.bind({});
GhostDisabled.args = {
  children: "Button",
  ghost: true,
  variant: "secondary",
  disable: true,
};

export const Override = Template.bind({});
Override.args = {
  children: "Button",
  override: "",
  //testes
};
