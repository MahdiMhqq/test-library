import { convertPxToRem, convertToRem, shadesGenerator } from "../../utils";
import defaultTheme from "./defaultTheme";
import {
  Theme,
  ThemeInput,
  IColorPalette,
  IColorInputPalette,
  IColor,
} from "./types";
import { IConfigs, IConfigsInput, WithUnit } from "./types/Configs";

// Translate Input Theme to config Theme
export const translateTheme = (inputTheme: ThemeInput | undefined) => {
  let theme: unknown = {};
  if (inputTheme) {
    // Translate
    const { colors: inputColors, configs: inputConfigs } = inputTheme;

    const translatedTheme = {
      ...defaultTheme,
      ...inputTheme,
      colors: {
        ...defaultTheme.colors,
        ...translateColors(inputColors),
      },
      configs: {
        ...defaultTheme.configs,
        ...translateConfigs(inputConfigs),
      },
    } satisfies Theme;

    theme = translatedTheme;
  } else theme = defaultTheme;

  // Config File to CSS Vars
  themeToVars(theme as Theme);

  return theme as Theme;
};

// Translate Colors
const translateColors = (inputColors?: IColorInputPalette) => {
  const colors: { [key: string]: IColor } = {};
  for (let key in inputColors) {
    const color = inputColors[key as keyof ThemeInput["colors"]];

    // Translation
    if (typeof color === "string") {
      const fullShade = shadesGenerator(color);
      colors[key] = {
        default: fullShade["500"],
        active: fullShade["600"],
        hover: fullShade["400"],
      };
    } else {
      colors[key] = color;
    }
  }
  return colors as unknown as IColorPalette;
};

// Translate Configs
const translateConfigs = (inputConfigs?: IConfigsInput) => {
  const configs: { [key: string]: number | string | WithUnit } = {};
  for (let key in configs) {
    const config = configs[key as keyof ThemeInput["configs"]];
    let remValue: number | null = null;

    // Translation
    if (typeof config === "number") remValue = convertPxToRem(config);
    else if (typeof config === "string") remValue = convertToRem(config);
    else {
      if (config[1] === "px") remValue = convertPxToRem(config[0]);
      else remValue = convertToRem(config[0]);
    }

    // Fill Translated Object
    if (remValue) configs[key] = remValue;
  }

  return configs as unknown as IConfigs;
};

const themeToVars = (theme: Theme) =>
  objTraverse(theme as unknown as MyObject, theme.varPrefix, createCssVar);

interface MyObject {
  [key: string | keyof MyObject | number]:
    | number
    | string
    | MyObject
    | Array<any>;
}

const objTraverse = (
  obj: MyObject,
  prefixValue: string,
  onFindNonObject: (
    key: string,
    prefixValue: string,
    value: number | string
  ) => void
) => {
  for (let key in obj) {
    const _value = key in obj ? obj[key] : null;
    if (_value) {
      if (typeof _value !== "object")
        key !== "varPrefix" && onFindNonObject(key, prefixValue, _value);
      else if (!Array.isArray(_value)) {
        const varPrefix = _value?.["varPrefix"] as string | undefined;
        objTraverse(
          _value,
          prefixValue + "-" + (varPrefix ?? key),
          onFindNonObject
        );
      }
      // To Do: Handle Array properties
    }
  }
};

const createCssVar = (
  key: string,
  prefixValue: string,
  value: number | string
) => {
  // @ts-ignore
  const clientDocument = document;
  if (clientDocument) {
    const root = clientDocument.documentElement;
    root.style.setProperty(prefixValue + "-" + key, value + "");
  }
};
