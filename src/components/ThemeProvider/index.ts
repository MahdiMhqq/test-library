// Modules
import defaultTheme from "./defaultTheme";

// Exports
export { default } from "./ThemeProvider";
export { defaultTheme };
export * from "./types";
