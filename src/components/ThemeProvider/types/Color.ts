import { OneLevelP } from "../../../utils/type";

export interface IColor {
  default: string;
  hover: string;
  active: string;
}

export type ColorInputSchema = IColor | string;

export interface IColorPalette {
  readonly varPrefix: string;

  // TailwindCSS default Theme colors
  gray: IColor;
  red: IColor;
  orange: IColor;
  yellow: IColor;
  green: IColor;
  teal: IColor;
  cyan: IColor;
  lightBlue: IColor;
  blue: IColor;
  indigo: IColor;
  purple: IColor;
  pink: IColor;
  rose: IColor;
  //slate

  // Mandatory theme colors
  primary: IColor;
  secondary: IColor;
  accent: IColor;
  surface: IColor;
  info: IColor;
  success: IColor;
  warning: IColor;
  error: IColor;
  base: IColor;
  white: IColor;
  black: IColor;
}

export type IColorInputPalette = Partial<
  OneLevelP<Omit<IColorPalette, "varPrefix">, string>
>;
