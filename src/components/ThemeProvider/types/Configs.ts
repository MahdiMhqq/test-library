import { OneLevelP } from "../../../utils/type";

export type WithUnit = [string | number, "rem" | "px"];

// NOTICE: All Configs and values will store as REM values.

export interface IConfigs {
  readonly varPrefix: string;

  // Configs
  defaultRadius: number;
}

export type IConfigsInput = Partial<
  OneLevelP<Omit<IConfigs, "varPrefix">, WithUnit | string>
>;
