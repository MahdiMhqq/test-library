/** Color ***********************/
import {
  ColorInputSchema,
  IColor,
  IColorInputPalette,
  IColorPalette,
} from "./Color";
import { IConfigs, IConfigsInput } from "./Configs";
export type { ColorInputSchema, IColor, IColorInputPalette, IColorPalette };

/** Theme ***********************/
// Input
export interface ThemeInput {
  colors?: IColorInputPalette;
  configs?: IConfigsInput;
}

// Configuration
export interface Theme {
  readonly varPrefix: string;
  colors: IColorPalette;
  configs: IConfigs;
}
