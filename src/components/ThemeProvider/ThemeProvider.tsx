import React from "react";

import { Theme, ThemeInput } from "./types";
import defaultTheme from "./defaultTheme";
import { translateTheme } from "./themeTranslator";

export const ThemeContext = React.createContext<Theme>(defaultTheme);

interface IThemeProvider {
  children: React.ReactNode;
  theme?: ThemeInput;
}

const ThemeProvider = ({ children, theme }: IThemeProvider) => {
  return (
    <ThemeContext.Provider value={translateTheme(theme)}>
      {children}
    </ThemeContext.Provider>
  );
};

export default ThemeProvider;
