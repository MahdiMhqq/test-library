import "../src/tailwind.css";

import React from "react";
import { ThemeProvider } from "../src";

export const decorators = [
  (Story) => (
    <ThemeProvider>
      <div style={{ padding: "1rem 1.5rem" }}>
        <Story />
      </div>
    </ThemeProvider>
  ),
];

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
  },
  backgrounds: {
    default: "SemiDark",
    values: [
      {
        name: "SemiDark",
        value: "#171E30",
      },
      {
        name: "White",
        value: "#FFF",
      },
      {
        name: "Black",
        value: "#000",
      },
    ],
  },
};
