/** @type {import('tailwindcss').Config} */

import safelistGenerator from "tailwind-safelist-generator";
import tailwindUtils from "./src/utils/tailwindcss.js";
const { generateColors } = tailwindUtils;

export default {
  content: ["./src/**/*.{js,jsx,ts,tsx}", "./safelist.txt"],
  theme: {
    colors: {
      transparent: "transparent",
      ...generateColors(),
    },
    extend: {},
  },
  plugins: [
    safelistGenerator({
      path: "safelist.txt",
      patterns: [
        "bg-{colors}",
        "hover:bg-{colors}",
        "active:bg-{colors}",
        "border-{colors}",
        "hover:border-{colors}",
        "active:border-{colors}",
        "text-{colors}",
        "hover:text-{colors}",
        "active:text-{colors}",
      ],
    }),
  ],
};
